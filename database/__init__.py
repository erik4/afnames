from peewee import *
from playhouse.sqlite_ext import SqliteExtDatabase
import datetime

db = SqliteExtDatabase('appdb.db')


class BaseModel(Model):
    class Meta:
        database = db


class ImportActies(BaseModel):
    id = PrimaryKeyField()
    naam = CharField(unique=True)
    datum = DateTimeField(default=datetime.datetime.now)


class Afnames(BaseModel):
    importactie = ForeignKeyField(ImportActies, related_name='afnames')
    id = PrimaryKeyField()
    naam = CharField()
    omschrijving = CharField()
    aanmeldnaam = CharField()
    weergavenaam = CharField()
    starttijd = DateTimeField()
    eindtijd = DateTimeField()
    cijfer = SmallIntegerField()
    score = SmallIntegerField
    maxscore = SmallIntegerField
    scoreperc = SmallIntegerField
    groepsnummer = CharField()

