from peewee import OperationalError
from database import db, ImportActies, Afnames


def initialise_app():

    db.connect()
    try:
        db.create_tables([ImportActies, Afnames])
    except OperationalError:
        # tabellen bestaan blijkbaar al.
        pass


def file_is_imported(filepath):
    """
    Check in the database if a file with this name was already imported.

    :param filepath: the full path to the file
    :return: an import date or None if the file is unknown
    """
    try:
        importactie = ImportActies.get(ImportActies.naam == filepath)
    except ImportActies.DoesNotExist:
        return None
    return importactie.datum


def import_csv_file(filepath):
    """
    Import a CSV file into the SQLite database

    :param filepath: the full path to the file
    :return: None
    """
    import csv

    ipa = ImportActies.create(naam=filepath)
    try:
        with open(filepath, newline='', encoding='ISO-8859-1') as csvfile:
            rdr = csv.reader(csvfile, delimiter=';')
            for row in rdr:
                if row[0][0] != '#':
                    Afnames.create(
                        importactie=ipa,
                        naam=row[0],
                        omschrijving=row[1],
                        aanmeldnaam=row[2],
                        weergavenaam=row[3],
                        starttijd=row[4],
                        eindtijd=row[5],
                        cijfer=row[6],
                        score=row[7],
                        maxscore=row[8],
                        scoreperc=row[9],
                        groepsnummer=row[10]
                    )
        print("{0} is geimporteerd.".format(filepath))
        return True
    except:
        print("{0} kon niet worden geimporteerd. Controleer of het formaat in orde is.".format(filepath))
        return False


def import_unknown_files():
    import os
    # import all CSV files
    valid_extentions = ['.csv']
    for fn in os.listdir('.'):
        if os.path.isfile(fn):
            ext = os.path.splitext(fn)[-1].lower()
            if ext in valid_extentions:
                importdate = file_is_imported(fn)
                if importdate is None:
                    import_csv_file(fn)


def find_student():
    qry = input("\r\nZoek: ")
    if len(qry) > 0:
        students = Afnames.select().join(ImportActies).where(
            (Afnames.aanmeldnaam.contains(qry)) |
            (Afnames.weergavenaam.contains(qry))
        )
        for s in students:
            print("gevonden: {0} - {1}".format(s.importactie.naam, s.weergavenaam, s.naam))
    else:
        print("Voer minimaal 1 karakter in.")


def main():
    print("""
         __                            _____  ____  
        / _|                          |  __ \|  _ \ 
   __ _| |_ _ __   __ _ _ __ ___   ___| |  | | |_) |
  / _` |  _| '_ \ / _` | '_ ` _ \ / _ \ |  | |  _ < 
 | (_| | | | | | | (_| | | | | | |  __/ |__| | |_) |
  \__,_|_| |_| |_|\__,_|_| |_| |_|\___|_____/|____/ 

    Serieus mooie software van Erik Oosterwaal.
    Druk op CTRL-C om te stoppen, als je kunt.    
    """)
    # create a sqlite database with the right tables, if not already present
    initialise_app()
    # import new files
    import_unknown_files()
    # search in the imported files
    try:
        while True:
            find_student()
    except KeyboardInterrupt:
        print('programma gestopt')



if __name__ == "__main__":
    main()

