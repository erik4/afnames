# Afnames

Afnames is not for public use or propably of use to anyone, but here for a specific assignment and for public reference only.
Therefore, the readme is in Dutch. If you can use any part of this software, good for you. Feel free to copy/use it.


# Marco:

Het programma is puur Python 3, maar kent wat afhankelijkheden. Wat je nodig hebt is:

  - Python 3.x geinstalleerd op een computer
  - de PeeWee bibliotheek in versie 2.10.2

Om te kijken welke python versie je hebt gebruik je:
```sh
python -v
```
Om PeeWee te installeren gebruik je het commando:
```sh
pip install peewee==2.10.2
```

Het werkt als volgt; start het programma met:
```sh
python app.py
```

Vervolgens gaat het programma eenmalig een database aanmaken in de map waar het draait. Die heet appdb.db en deze kun je openen met een willekeurige tool voor SQLite. (SqLite manager, SQLite studio, etc.)

Het programma gaat vervolgens op zoek naar CSV bestanden in dezelfde map. Als het die vindt, en het formaat komt overeen met het voorbeeldbestand wat ik heb gekregen, dan wordt de data automatisch geimporteerd in de SQLite database. Als het bestand al eerder geimporteerd is, dan niet. Je kan reeds geimporteerde bestanden dus gewoon laten staan, hij importeert alleen nieuwe bestanden.

Tot slot komt er een zoek: prompt in beeld waar je (delen van) inschrijfnaam in kunt vullen. De database wordt doorzocht en meldt gevonden studenten, inclusief het bestand waar ze origineel in gevonden zijn.

p.s.: in het voorbeeldbestand was de eerste regel voorzien van een '#'. Ik ben ervan uitgegaan dat dat een markering is die aangeeft dat de regel geen data bevat, maar kopteksten. De software gaat er dus van uit dat de regels in de CSV die geen echte data bevatten vooraf gegaan worden door een '#'.

Laat maar even weten als je er niet uitkomt of andere vragen hebt.
Ik kan er een executable voor Mac of PC van maken, maar als dit zo werkt, ben je sneller op weg, en kun je zelf nog wat aanpassen als je wilt.

Groeten,
Erik
